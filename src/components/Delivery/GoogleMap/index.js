// import React, { Component } from 'react';
// import GoogleMapReact from 'google-map-react';
// import Marker from './Marker'


// class SimpleMap extends Component {

//   static defaultProps = {
//     center: {
//       lat: JSON.parse(localStorage.getItem("currentPositionUser")).lat,
//       lng: JSON.parse(localStorage.getItem("currentPositionUser")).long
//     },
//     zoom: 16
//   };
//   state = {
//     directions: null,
//     error: null

//   }

//   componentDidMount() {

//     navigator.geolocation.getCurrentPosition(
//       function(position) {
//         let lat = position.coords.latitude;
//         let long = position.coords.longitude;
//         let userPosition = {"lat" : lat, 'long' : long}
//         var personJSONString=JSON.stringify(userPosition);
//         localStorage.setItem("currentPositionUser",personJSONString);
//         // let getUserLocation = JSON.parse(localStorage.getItem("currentPositionUser"));
//       },
//       function(error) {
//         console.error("Error Code = " + error.code + " - " + error.message);
//       }
//     );

//   }

//   render() {

//     return (
//       // Important! Always set the container height explicitly
//       <div style={{ height: '100vh', width: '100%' }}>
//         <GoogleMapReact
//           bootstrapURLKeys={{ key: 'AIzaSyDZGm6DIcj1-wBewxvkH9MLTPgZsyVaEYA' }}
//           defaultCenter={this.props.center}
//           defaultZoom={this.props.zoom}
//         >
//           <Marker
//             lat={JSON.parse(localStorage.getItem("currentPositionUser")).lat}
//             lng={JSON.parse(localStorage.getItem("currentPositionUser")).long}
//             text="My Marker"
//           />
//         </GoogleMapReact>
//       </div>
//     );
//   }
// }

// export default SimpleMap;




import Marker from './Marker'

import React from "react";
import {
  GoogleMap,
  withGoogleMap,
  withScriptjs,
  DirectionsRenderer
} from "react-google-maps";
import { withProps, compose, lifecycle } from "recompose";
const google = window.google = window.google ? window.google : {}

const MapWithADirectionsRenderer = compose(

  withProps({
    googleMapURL:
      "https://maps.googleapis.com/maps/api/js?key=AIzaSyDZGm6DIcj1-wBewxvkH9MLTPgZsyVaEYA&v=3.exp&libraries=geometry,drawing,places",
    loadingElement: <div style={{ height: `100%` }} />,
    containerElement: <div style={{ height: `100vh` }} />,
    mapElement: <div style={{ height: `100%` }} />
  }),
  withScriptjs,
  withGoogleMap,
  lifecycle({
    componentDidMount() {

      const DirectionsService = new google.maps.DirectionsService();
      navigator.geolocation.getCurrentPosition(
        function (position) {
          let lat = position.coords.latitude;
          let long = position.coords.longitude;
          let userPosition = { "lat": lat, 'long': long }
          var personJSONString = JSON.stringify(userPosition);
          localStorage.setItem("currentPositionUser", personJSONString);
          // let getUserLocation = JSON.parse(localStorage.getItem("currentPositionUser"));

        },
        function (error) {
          console.error("Error Code = " + error.code + " - " + error.message);
        },
        { enableHighAccuracy: true }
      );

      DirectionsService.route(
        {
          origin: new google.maps.LatLng(JSON.parse(localStorage.getItem("currentPositionUser")).lat, JSON.parse(localStorage.getItem("currentPositionUser")).long),
          destination: new google.maps.LatLng(12.9815, 80.2180),
          travelMode: google.maps.TravelMode.DRIVING,
          provideRouteAlternatives: true,
          optimizeWaypoints: true

        },
        (result, status) => {
          if (status === google.maps.DirectionsStatus.OK) {
            this.setState({
              directions: result
            });
          } else {
            console.error(`error fetching directions ${result}`);
          }
        }
      );
    }
  })
)(props => (
  <GoogleMap
    // defaultZoom={20}
    defaultOptions={{
      mapTypeControl: false,
      streetViewControl: true,
      zoomControl: true,
      fullscreenControl: false,
      minZoom: 15,
      maxZoom: 20,
    
    }}

    defaultCenter={new google.maps.LatLng(JSON.parse(localStorage.getItem("currentPositionUser")).lat, JSON.parse(localStorage.getItem("currentPositionUser")).long)}
  >
    {props.directions && 
    <DirectionsRenderer
      options={{
        polylineOptions: {
          stokeColor: "#ff0000",
          strokeOpacity: 0.3,
          strokeWeight: 4,
          visible:true
        },
        markerOptions:{
          icon : {            
            url: '/assets/img/various/delivery-map-marker.png',
          },
        }
      }}
      directions={props.directions} />}
  </GoogleMap>
));

class Map extends React.Component {
  render() {
    return (
      <React.Fragment>
        <MapWithADirectionsRenderer />
      </React.Fragment>
    );
  }
}

export default Map;
