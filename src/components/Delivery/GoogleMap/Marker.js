import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
// import myImage from '../../assets/img/various/cod.png';

const Wrapper = styled.div`
  position: absolute;
  top: 50%;
  left: 50%;
  width: 60px;
  height: 60px;
  // background-color: #000;
  background-image : url(${'/assets/img/various/delivery-map-marker.png'});
  // border: 2px solid #fff;
  // border-radius: 100%;
  user-select: none;
  background-repeat: no-repeat;
  transform: translate(-50%, -50%);
  // cursor: ${(props) => (props.onClick ? 'pointer' : 'default')};
  &:hover {
    z-index: 1;
  }
`;

const Marker = ({ text, onClick }) => (
  <Wrapper
    alt={text}
    onClick={onClick}
  />
);

Marker.defaultProps = {
  onClick: null,
};

Marker.propTypes = {
  onClick: PropTypes.func,
  text: PropTypes.string.isRequired,
};

export default Marker;
