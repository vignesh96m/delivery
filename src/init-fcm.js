import "firebase/messaging";

import * as firebase from "firebase/app";

let messaging;

if (firebase.messaging.isSupported()) {
	if (localStorage.getItem("firebasePublic") !== null && localStorage.getItem("firebaseSenderId") !== null) {
		const initializedFirebaseApp = firebase.initializeApp({
			messagingSenderId: localStorage.getItem("firebaseSenderId"),
		});
		messaging = initializedFirebaseApp.messaging();
		// messaging.usePublicVapidKey(localStorage.getItem("firebasePublic"));
		messaging.usePublicVapidKey(
			"BMdqVXTnkDNUqTQOJn0WCZFoCxXJZoFcYs1q4FvKC9e5KH7TGvY7XUKhZPMlBkTIatnnfIMV2ffrQYu12QmeD2M"
		);
	} else {
		const initializedFirebaseApp = firebase.initializeApp({
			messagingSenderId: "294770777144",
		});
		messaging = initializedFirebaseApp.messaging();
		messaging.usePublicVapidKey(
			"BMdqVXTnkDNUqTQOJn0WCZFoCxXJZoFcYs1q4FvKC9e5KH7TGvY7XUKhZPMlBkTIatnnfIMV2ffrQYu12QmeD2M"
		);
	}
}
export default messaging;
