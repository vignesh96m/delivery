// Use this for Local Development
// export const WEBSITE_URL = "http://127.0.0.1:8000";

// export const WEBSITE_URL = "https://lataintl.com/bom";

export const WEBSITE_URL = "https://bom.prpo.com.my";

// Comment out the above and Uncomment the below line before building the files for Production
// export const WEBSITE_URL = "https://" + window.location.hostname;
